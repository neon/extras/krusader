Source: krusader
Section: kde
Priority: optional
Maintainer: Debian Qt/KDE Maintainers <debian-qt-kde@lists.debian.org>
Uploaders: Mark Purcell <msp@debian.org>, Maximiliano Curia <maxy@debian.org>
Build-Depends: cmake,
               debhelper-compat (= 13),
               gettext,
               kf6-extra-cmake-modules,
               kf6-karchive-dev,
               kf6-kbookmarks-dev,
               kf6-kcodecs-dev,
               kf6-kcompletion-dev,
               kf6-kconfig-dev,
               kf6-kcoreaddons-dev,
               kf6-kdoctools-dev,
               kf6-kguiaddons-dev,
               kf6-ki18n-dev,
               kf6-kiconthemes-dev,
               kf6-kitemviews-dev,
               kf6-knotifications-dev,
               kf6-kparts-dev,
               kf6-kstatusnotifieritem-dev,
               kf6-ktextwidgets-dev,
               kf6-kwallet-dev,
               kf6-kwidgetsaddons-dev,
               kf6-kwindowsystem-dev,
               kf6-kxmlgui-dev,
               kf6-solid-dev,
               libacl1-dev,
               libattr1-dev,
               pkg-kde-tools-neon,
               qt6-base-dev,
               zlib1g-dev,
Standards-Version: 4.6.2
Rules-Requires-Root: no
Homepage: https://www.krusader.org
Vcs-Browser: https://salsa.debian.org/qt-kde-team/extras/krusader
Vcs-Git: https://salsa.debian.org/qt-kde-team/extras/krusader.git

Package: krusader
Architecture: any
Depends: ${misc:Depends}, ${shlibs:Depends}
Recommends: kio-extras
Suggests: arj,
          ark,
          bzip2,
          cpio,
          kate,
          kdiff3 | kompare | xxdiff,
          kmail,
          konsole,
          krename,
          lha,
          md5deep | cfv,
          okteta,
          p7zip,
          rpm,
          unace,
          unrar | unrar-free | rar [amd64 i386],
          unzip,
          zip,
Description: twin-panel (commander-style) file manager
 Krusader is a simple, easy, powerful, twin-panel (commander-style) file
 manager, similar to Midnight Commander (C) or Total Commander (C).
 .
 It provides all the file management features you could possibly want.
 .
 Plus: extensive archive handling, mounted filesystem support, FTP,
 advanced search module, viewer/editor, directory synchronisation,
 file content comparisons, powerful batch renaming and much more.
 .
 It supports archive formats: ace, arj, bzip2, deb, iso, lha, rar, rpm, tar,
 zip and 7-zip.
 .
 It handles KIOSlaves such as smb:// or fish://.
 .
 Almost completely customizable, Krusader is very user friendly, fast and looks
 great on your desktop.
