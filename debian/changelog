krusader (2:2.9.0-0neon) noble; urgency=medium

  * New release

 -- Neon CI <neon@kde.org>  Thu, 02 Jan 2025 01:00:13 +0000

krusader (2:2.8.1-0neon) jammy; urgency=medium

  [ Jonathan Esk-Riddell ]
  * New release

  [ Neon CI ]
  * New release

 -- Neon CI <neon@kde.org>  Mon, 18 Mar 2024 01:00:19 +0000

krusader (2:2.7.2-2) unstable; urgency=medium

  * Team upload.

  [ Aurélien COUDERC ]
  * Use canonical Salsa URLs in control file.

  [ Pino Toscano ]
  * Add the configuration for the CI on salsa.
  * Bump the debhelper compatibility to 13:
    - switch the debhelper build dependency to debhelper-compat 13
    - remove debian/compat
  * Remove the explicit as-needed linking, as it is done by binutils now.
  * Explicitly add the gettext build dependency.
  * Add Rules-Requires-Root: no.
  * Use secure URI in Homepage field.
  * Fix day-of-week for changelog entry 1.60.0-2.
  * Update standards version to 4.5.0, no changes needed.
  * Set upstream metadata fields: Repository, Repository-Browse.
  * Remove outdated Source in copyright.

 -- Pino Toscano <pino@debian.org>  Thu, 12 Nov 2020 11:48:12 +0100

krusader (2:2.7.2-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * Bump the qtbase5-dev build dependency to 5.9, according to the upstream
    build system.
  * Add the libacl1-dev, and libattr1-dev build dependencies to enable ACL
    support in file synchronization.
  * Link in as-needed mode.
  * Bump Standards-Version to 4.4.0, no changes required.
  * Stop installing the TODO file as documentation, as it is not useful for
    users.
  * Limit the rar suggest only on amd64, and i386, i.e. the only architectures
    where it exists.

 -- Pino Toscano <pino@debian.org>  Sun, 15 Sep 2019 08:22:16 +0200

krusader (2:2.7.1-1) unstable; urgency=medium

  * Update Maintainer, alioth is gone
  * Update source url in the debian/copyright
  * New upstream release (2.7.1).
  * Release to unstable

 -- Maximiliano Curia <maxy@debian.org>  Sat, 25 Aug 2018 10:20:12 +0200

krusader (2:2.7.0-1) unstable; urgency=medium

  * New upstream release (2.7.0).
  * Update uploaders list as requested by MIA team (Closes: #879370)
  * Add kde-cli-tools as recommends.
    kde-cli-tools ships kdesu which is needed for the "Start Root Mode
    Krusader" menu option.
  * Update recommends/suggests with upstream's INSTALL
  * Update Vcs fields
  * Bump Standards-Version to 4.1.4.
  * Use https url in the debian/copyright Format
  * Migrate to dh using compat 11.
    Also drop parallel option, no longer needed for dh >= 11, and list-missing,
    not really needed as we don't have an install file.
  * Drop dh_auto_test override
  * Release to unstable

 -- Maximiliano Curia <maxy@debian.org>  Tue, 15 May 2018 09:59:54 +0200

krusader (2:2.6.0-1) unstable; urgency=medium

  * New upstream release (2.6.0).
  * Bump Standards-Version to 4.0.0, no changes needed.
  * Update Vcs fields, migrate to git
  * Migrate to automatic dbgsym packages
  * debian/watch: Use https
  * Drop upstream patches
  * Update build-deps and deps with the info from cmake
  * Add a .gitattributes file to use dpkg-mergechangelogs
  * Release to unstable

 -- Maximiliano Curia <maxy@debian.org>  Wed, 12 Jul 2017 17:45:30 +0200

krusader (2:2.5.0-2) unstable; urgency=medium

  * Add missing kinit runtime dependency.
  * Add NEWS entry for krusader configuration paths change. (Closes: 854992)
    Thanks Roman Lebedev for reporting.
  * Add patches to restore minimize tray function. (Closes: 848711)
     + ADDED-Reimplementation-of-minimize-to-system-tray-feature.patch
     + FIXED-365105-Workaround-for-bug-in-KF5-QSystemTrayIcon.patch
    Thanks Andrej Mernik for reporting.

 -- Maximiliano Curia <maxy@debian.org>  Sat, 11 Feb 2017 10:39:05 +0100

krusader (2:2.5.0-1) unstable; urgency=medium

  * New upstream release (2.5.0)
  * Update the install file
  * Add new patch: Build-and-install-the-manpages.patch
  * Bump Standards-Version to 3.9.8, no changes needed.

 -- Maximiliano Curia <maxy@debian.org>  Wed, 09 Nov 2016 16:18:11 +0100

krusader (1:15.08~2015.11.01-1) experimental; urgency=medium

  * New upstream pre release (15.08~2015.11.01)
  * Update build dependencies.
  * Update copyright information.
  * Update install files.

 -- Maximiliano Curia <maxy@debian.org>  Wed, 04 Nov 2015 16:36:40 +0100

krusader (1:2.6.0-0neon) xenial; urgency=medium

  * New release

 -- Neon CI <neon@kde.org>  Thu, 13 Apr 2017 12:34:49 +0000

krusader (1:2.5.0-0neon) xenial; urgency=medium

  * Bump epoch

 -- Raymond Wooninck <tittiatcoke@gmail.com>  Thu, 27 Oct 2016 09:57:30 +0200

krusader (1:2.4.0~beta3-2) unstable; urgency=low

  * Upload to unstable

 -- Mark Purcell <msp@debian.org>  Sun, 12 May 2013 09:45:27 +1000

krusader (1:2.4.0~beta3-1) experimental; urgency=low

  * New upstream release
  * debian/watch -> check for .bz2
  * debian/compat -> 9
  * ACK NMUs - Thanks modax & doko

 -- Mark Purcell <msp@debian.org>  Fri, 29 Mar 2013 09:25:09 +1100

krusader (1:2.3.0~beta1-1+wheezy3) testing-proposed-updates; urgency=low

  * Unbump debhelper compat level (back to 7).
  * Take C(,XX,PP)FLAGS, LDFLAGS from dpkg-buildflags and export them to the
     build environment.

 -- Modestas Vainius <modax@debian.org>  Sun, 07 Oct 2012 21:12:44 +0300

krusader (1:2.3.0~beta1-1+wheezy2) testing-proposed-updates; urgency=low

  * Bump debhelper compat level to 9 in order to pull in standard build flags.
     As a side effect, this fixes FTBFSes on arm*.

 -- Modestas Vainius <modax@debian.org>  Sat, 06 Oct 2012 11:12:12 +0300

krusader (1:2.3.0~beta1-1+wheezy1) testing-proposed-updates; urgency=low

  * Backport a patch from upstream git to fix a FTBFS with gcc 4.7.
     (Closes: #684843, #667231) Patch: fix_gcc4.7_ftbfs.diff.

 -- Modestas Vainius <modax@debian.org>  Sat, 29 Sep 2012 23:01:06 +0300

krusader (1:2.4.0~beta1+git76+de1ea35-1.1) unstable; urgency=low

   * Non maintainer upload
   * Work around build failure with GCC 4.7. Closes: #667231.

 -- Matthias Klose <doko@debian.org>  Tue, 22 May 2012 11:36:40 +0000

krusader (1:2.4.0~beta1+git76+de1ea35-1) unstable; urgency=low

  * New snaphshot.
  * Update debian/compat: bump to 8.
  * Update debian/control:
    - bump Standards-Version to 3.9.2 - no changes needed.
    - drop kedit from suggests. (Closes: #646414)

 -- Fathi Boudra <fabo@debian.org>  Sat, 31 Dec 2011 15:12:47 +0200

krusader (1:2.3.0~beta1-1) unstable; urgency=low

  * New upstream release.
  * Drop rev1154581.patch - cherry-picked upstream.

 -- Fathi Boudra <fabo@debian.org>  Sat, 25 Dec 2010 16:25:48 +0200

krusader (1:2.2.0~beta1-2) unstable; urgency=medium

  * Grab upstream rev1154581.patch
    - Fixes "Can't stop krusader" (Closes: #587842)
  * Urgency=medium for RC bug

 -- Mark Purcell <msp@debian.org>  Sat, 09 Oct 2010 11:36:41 +1100

krusader (1:2.2.0~beta1-1) unstable; urgency=low

  * New upstream release.
  * Update location of KDE 4 HTML documentation.

 -- Fathi Boudra <fabo@debian.org>  Wed, 02 Jun 2010 01:04:52 +0300

krusader (1:2.1.0~beta1-1) unstable; urgency=low

  * New upstream release:
    - Fix FTBFS with GCC 4.4 (Closes: #526164). Thanks to Martin Michlmayr.
    - Fix FTBFS with binutils-gold (Closes: #555061).
  * Update debian/control:
    - Drop cdbs build dependency.
    - Bump build dependencies version: debhelper, pkg-kde-tools and
      kdelibs5-dev.
    - Bump Standards-Version to 3.8.3 (no changes needed).
  * Update debian/rules: convert to dh usage.

 -- Fathi Boudra <fabo@debian.org>  Wed, 11 Nov 2009 13:06:50 +0100

krusader (1:2.0.0-1) unstable; urgency=low

  * New upstream release
    - Add 1: epoc to increment over 2.0.0beta
    - Fixes "2.0.0 version allready available" (Closes: #524738)
    - Fixes "krusader crashes in FTP" (Closes: #420573)
    - Fixes "Krusader ftp is bad" (Closes: #358606)
    - Fixes "viewer crashes on special occasions" (Closes: #365279)
    - Fixes "wrong url for remote-address with external editor" (Closes: #386329)
    - Fixes "permissions of ~/.ICEAuthority" (Closes: #417797)
    - Fixes "FTP crashing problem" (Closes: #420566)
    - Fixes "fetch password from KWallet" (Closes: #444746)
    - Fixes "viewer won't show text" (Closes: #357598)
    - Fixes "panel doesn't update" (Closes: #357920)
    - Fixes "takes long to start up" (Closes: #383475)
    - Fixes "Secure Passwords - KDE Wallet" (Closes: #460322)
    - Fixes "Make KrViewer usable for big pictures" (Closes: #482418)
    - Fixes "Please make FTP connection manager" (Closes: #500689)
  * Update debian/watch to handle upstream -beta
  * Remove Build-Depends: quilt - quilt-build-dep-but-not-series-file
  * Add ${misc:Depends}

 -- Mark Purcell <msp@debian.org>  Mon, 20 Apr 2009 08:42:08 +1000

krusader (2.5.0-0neon) xenial; urgency=medium

  * New release

 -- Neon CI <neon@kde.org>  Wed, 26 Oct 2016 18:44:43 +0000

krusader (2.4.0-0neon) xenial; urgency=medium

  * Initial release.

 -- Raymond Wooninck <tittiatcoke@gmail.com>  Sat, 08 Oct 2016 17:08:51 +0200
krusader (2.0.0beta2+svn6253-1) unstable; urgency=low

  * Upload to Sid.
  * Bump compat/debhelper to 7.
  * Bump Standard-Version to 3.8.1 (no changes needed).
  * New snasphot from svn revision 6253.
  * Build against kdelibs5-dev (>= 4.2.0).
  * Switch to pkg-kde-tools.

 -- Fathi Boudra <fabo@debian.org>  Tue, 07 Apr 2009 11:13:44 +0200

krusader (2.0.0beta2-1) experimental; urgency=low

  * New upstream release.

 -- Fathi Boudra <fabo@debian.org>  Mon, 29 Dec 2008 10:59:58 +0100

krusader (2.0.0beta1+svn6114-1) experimental; urgency=low

  * New snasphot from svn revision 6114.

 -- Fathi Boudra <fabo@debian.org>  Mon, 10 Nov 2008 11:58:19 +0100

krusader (2.0.0beta1+svn6078-1) experimental; urgency=low

  * Initial KDE 4 release.
  * Bump compat/debhelper to 6.
  * Adjust build dependencies to new build system.
  * Update Homepage field.
  * Remove menu dependency and menu file.
  * Replace khexedit suggestion by okteta.
  * Update copyright: Add Vaclav Juza to authors list.

 -- Fathi Boudra <fabo@debian.org>  Sun, 07 Sep 2008 13:41:43 +0200

krusader (1.90.0-3) unstable; urgency=low

  * Unbump compat/debhelper to 6. Switch back to 5 for Lenny.
  * Switch back to cdbs simple patch system. Remove quilt build dependency.

 -- Fathi Boudra <fabo@debian.org>  Sun, 10 Aug 2008 21:15:10 +0200

krusader (1.90.0-2) unstable; urgency=low

  * Add krusader branch pull patch:
    * Update french and ukrainian translations.
    * Update credits in the About box.
    * Fix Availability restrictions of UserActions. (Closes: #492955)
  * Bump compat/debhelper to 6.
  * Switch to quilt patch system: add quilt build dependency.
  * Use su-to-root instead of kdesu. Add menu dependency. (Closes: #479821)
  * Bump Standard-Version to 3.8.0 (no changes needed).
  * Use Vcs-Browser and Vcs-Svn fields.

 -- Fathi Boudra <fabo@debian.org>  Sun, 10 Aug 2008 17:00:07 +0200

krusader (1.90.0-1) unstable; urgency=low

  [ Ana Beatriz Guerrero Lopez ]

  * New upstream release:
    - GCC 4.3 compatibility (Closes: #455657)
  * Update Debian menu section to new policy.
  * Replace GFDL full license text in debian/copyright for a pointer
    to /usr/share/common-licenses/GFDL-1.2

  [ Fathi Boudra ]

  * Update my e-mail address.
  * Remove Angel Ramos from Uploaders field.
  * Bump Standard-Version to 3.7.3.
  * Add kdebase-bin versioned dependency.

 -- Debian KDE Extras Team <pkg-kde-extras@lists.alioth.debian.org>  Sun, 23 Mar 2008 15:10:45 +0100

krusader (1.80.0-1) unstable; urgency=low

  * New upstream release.

 -- Fathi Boudra <fboudra@free.fr>  Sun, 22 Jul 2007 20:22:02 +0200

krusader (1.80.0~beta2-1) unstable; urgency=low

  * New upstream release.

  [Fathi Boudra]
  * Update copyright.
  * Remove gcc4.2 support patch. Merged upstream.
  * Update useractions examples patch.

  [Mark Purcell]
  * Update debian/watch.
  * Robust get-orig-source target.

 -- Fathi Boudra <fboudra@free.fr>  Fri, 11 May 2007 18:12:49 +0200

krusader (1.80.0~beta1-1) experimental; urgency=low

  * Add debug package
  * Add automake1.9 to Build-Depends
  * Clean up rules:
    * remove DEB_BUILDDIR
    * add DEB_DESTDIR
    * remove desktop files freedesktop compliant hack
  * Update watch

 -- Fathi Boudra <fboudra@free.fr>  Sun,  7 Jan 2007 16:11:37 +0100

krusader (1.70.1-1) unstable; urgency=low

  * New upstream release
    * fix cleartext in the bookmark file CVE-2006-3816 (Closes: #380063)
  * Convert package to cdbs
  * New maintainers:
    * Maintainer: KDE Extras Team
    * Uploaders: Angel Ramos, Mark Purcell and Fathi Boudra
  * compat: bumped to 5
  * control:
    * removed unneeded Build-depends
    * bumped Standards-Version to 3.7.2
    * Suggests updated
    * Description updated
  * copyright: added missing copyrights and documentations license
  * docs: added FAQ
  * Added watch file
  * Added some popular useractions to Useractions.xml, patch by Vaclav Juza
  * Added krusader-rootmode to debian menu,thanks to Vaclav Juza
  * Added gcc 4.2 support patch by Martin Michlmayr (Closes: #361933)
  * Fix desktop files path to be freedesktop compliant (Closes: #378138)

 -- Fathi Boudra <fboudra@free.fr>  Sat, 19 Aug 2006 12:37:05 +0200

krusader (1.70.0-1) unstable; urgency=low

  * New upstream release (Closes: #352574, #336169, #343960, #342717, #349874, #351527, #345097, #345098, #349873, #328466, #352367, #347126).
  * It's not clear rar/unrar suggest violates DFSG #4 (Closes: #340706).
  * Krusader doesn't crash now (Closes: #349784, #350890, #355464, #355871).
  * Krusader new upstream doesn't crash viewing .gz files (Closes: #288661).
  * Viewer Window too big appears not be caused by krusader (Closes: #349870).
  * Imposible reproduce white stripe problem (Closes: #349872).
  * Added libkjsembed-dev and libkonq4-dev as build-depends (Closes: #331130).

 -- Angel Ramos <seamus@debian.org>  Sat, 11 Mar 2006 16:43:05 +0100

krusader (1.60.0-3.1) unstable; urgency=low

  * Non-maintainer upload.
  * Remove Build-Dependency on xlibs-dev (Closes: #347126).
  * Credit and Big Thanks to Justin Pryzby <justinpryzby@users.sourceforge.net>
    for the patch and testing.

 -- Amaya Rodrigo Sastre <amaya@debian.org>  Sat, 21 Jan 2006 21:01:08 +0100

krusader (1.60.0-3) unstable; urgency=low

  * Fixed gcc-4.0 compiling problem (Closes: #326789).
  * Fixed autoconf m68k and mipsel problem (Closes: #310669).
  * de.po corrected (Closes: #313785).
  * Added kdebase-kio-plugins as suggest header (Closes: #322764).
  * The problem with ps/pdf files is a kghostview problem, not
    krusader (Closes: #325130)
  * No segfault on closing the viewer with krusader 1.60 (Closes: #325170).
  * No crash when user switches desktops on krusader 1.60 (Closes: #320297).

 -- Angel Ramos <seamus@debian.org>  Mon, 12 Sep 2005 17:25:48 +0100

krusader (1.60.0-2) unstable; urgency=low

  * Fixed FTBFS on all archs (Closes: #306756).

 -- Angel Ramos <seamus@debian.org>  Tue, 24 May 2005 17:38:48 +0200

krusader (1.60.0-1) unstable; urgency=low

  * New upstream release (Closes: #305494).
  * Package description and suggests update. Thanks Frank Schoolmeesters
    for the help and reports! (Closes: #291617, #297991).
  * Fixed problem opening documentation (Closes: #291611).
  * Not reporting read-only fs is a kde problem not krusader
    (Closes: #291811).

 -- Angel Ramos <seamus@debian.org>  Wed, 27 Apr 2005 18:12:48 +0200

krusader (1.51-1) unstable; urgency=low

  * New upstream release (Closes: #280037, #287015).
  * Moved from section utils to kde (Closes: #286748).
  * Renamed dk.po to da.po (Closes: #269414).

 -- Angel Ramos <seamus@debian.org>  Thu, 30 Dec 2004 16:18:26 +0100

krusader (1.40-1) unstable; urgency=low

  * New upstream release (Closes: #260824).

 -- Angel Ramos <seamus@debian.org>  Wed, 18 Aug 2004 17:20:27 +0200

krusader (1.30-2) unstable; urgency=low

  * Renamed dk.po to da.po (Closes: #238872).
  * Updated libtool (Closes: #246354).
  * x-ace.desktop problems solved (Closes: #245663, #238052).

 -- Angel Ramos <seamus@debian.org>  Fri, 30 Apr 2004 18:35:00 +0200

krusader (1.30-1.2) unstable; urgency=low

  * NMU to fix broken previous NMU
  * really remove x-ace.desktop this time

 -- Andreas Barth <aba@not.so.argh.org>  Fri, 23 Apr 2004 07:49:35 +0200

krusader (1.30-1.1) unstable; urgency=low

  * NMU during BSP
  * remove x-ace.desktop, which is also in kdelibs-data. Closes: #238052

 -- Andreas Barth <aba@not.so.argh.org>  Sun, 18 Apr 2004 11:39:32 +0200

krusader (1.30-1) unstable; urgency=low

  * New upstream release (Closes: #201496).
  * Remove unzip-crypt and zip-crypt from the Suggests line
    (Closes: #190765).
  * Fixed segfault when trying to view files with no read access
    (Closes: #198298).

 -- Angel Ramos <seamus@debian.org>  Mon,  5 Jan 2004 21:06:23 +0100

krusader (1.11-1) unstable; urgency=low

  * New upstream release.
  * Updated config.guess and config.sub (Closes: #168658).

 -- Angel Ramos <seamus@debian.org>  Thu,  27 Feb 2003 16:31:00 +0200

krusader (1.00-2) unstable; urgency=low

  * Added autoconf2.13 as build dep in order the package can be built
    (Closes: #164973).
  * Added kmail and xxdiff as suggest (Closes: #153999).

 -- Angel Ramos <seamus@debian.org>  Wed,  23 Oct 2002 21:35:06 +0200

krusader (1.00-1) unstable; urgency=low

  * New upstream release (Closes: #127984).
  * Fixed admin/debianrules on the source.

 -- Angel Ramos <seamus@debian.org>  Tue,  8 Jan 2002 10:50:06 +0100

krusader (0.99-1.1) unstable; urgency=low

  * Fixed spelling error in description (Closes: #124850).

 -- Angel Ramos <seamus@debian.org>  Tue, 18 Dec 2001 02:07:00 +0100

krusader (0.99-1) unstable; urgency=low

  * New upstream release

 -- Angel Ramos <seamus@debian.org>  Tue, 11 Dec 2001 13:13:41 +0100

krusader (0.98-3) unstable; urgency=low

  * Added automake and autoconf as build-deps to solve the build problems
    on some architectures (Closes: #121664).

 -- Angel Ramos <seamus@debian.org>  Thu, 29 Nov 2001 10:37:20 +0100

krusader (0.98-2) unstable; urgency=low

  * Changes on debian/rules in order to add right alpha compiler flags
    for alpha builds (Closes: #121376).

 -- Angel Ramos <seamus@debian.org>  Wed, 28 Nov 2001 11:48:03 +0100

krusader (0.98-1) unstable; urgency=low

  * New upstream release

 -- Angel Ramos <seamus@debian.org>  Tue, 30 Oct 2001 11:42:37 +0100

krusader (0.97-1) unstable; urgency=low

  * New upstream release
  * Solved problems with broken docbook file (Closes: #115337).

 -- Angel Ramos <seamus@debian.org>  Mon, 15 Oct 2001 13:16:38 +0200

krusader (0.95-3) unstable; urgency=low

  *Fixed config.sub and config.guess on the source in order to
   support hppa architecture (Closes: #108276).
  *Symlink from locolor icon to highcolor created (Closes: #110453).

 -- Angel Ramos <seamus@debian.org>  Wed, 29 Aug 2001 10:12:22 +0200

krusader (0.95-2) unstable; urgency=low

  *Fixed architecture bug on control file (Closes: #110344).
  *Solved some problems with wrong link files.

 -- Angel Ramos <seamus@debian.org>  Tue, 28 Aug 2001 09:35:49 +0200

krusader (0.95-1) unstable; urgency=low

  *New Maintainer (Closes: #84536).
  *New upstream release.

 -- Angel Ramos <seamus@debian.org>  Fri, 24 Aug 2001 13:05:22 +0200

krusader (0.75-2) unstable; urgency=low

  * Maintainer set to Debian QA Group <packages@qa.debian.org>.
  * Added a build dependency on automake.

 -- Adrian Bunk <bunk@fs.tum.de>  Wed, 22 Aug 2001 23:56:27 +0200

krusader (0.75-1.2) unstable; urgency=low

  * NMU
  * Build against new qt/kdelibs
  * update build-depends

 -- Ivan E. Moore II <rkrusty@debian.org>  Tue, 27 Feb 2001 13:35:16 -0700

krusader (0.75-1.1) unstable; urgency=low

  * Rebuilt package to remove wrong dependency on xlibs (Closes: #80199).

 -- Martin Michlmayr <tbm@cyrius.com>  Thu, 21 Dec 2000 19:39:16 +0100

krusader (0.75-1) unstable; urgency=low

  * New upstream release.
  * Fixed install destinations in misc/Makefile.am.
  * Minor changes in rules.
  * Changed title in krusader.doc-base.
  * Build-Depends: removed docbook, docbook-stylesheets, jade and
    sgml-base (not used now).
  * Recommends: added tar and gzip.
  * Upload sponsored by Martin Michlmayr <tbm@debian.org>.

 -- Mariusz Przygodzki <dune@home.pl>  Mon, 11 Dec 2000 18:49:22 +0100

krusader (0.70-1) unstable; urgency=low

  * Initial Release. Closes: #75893
  * Changed krusader.desktop and its location.
  * Upload sponsored by Martin Michlmayr <tbm@debian.org>.

 -- Mariusz Przygodzki <dune@home.pl>  Sat,  9 Dec 2000 15:57:15 +0100
